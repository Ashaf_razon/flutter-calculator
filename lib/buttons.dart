import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {

  final color;
  final textColor;
  final String buttonText;
  final buttOnTapped;

//Constructor
  MyButton({this.color, this.textColor, this.buttonText, this.buttOnTapped});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: buttOnTapped,
      child: Padding(
        padding: const EdgeInsets.all(0.2),
        child: ClipRRect(
          //borderRadius: BorderRadius.circular(2),
          child: Container(
            color: color,
            child: Center(
              child: Text(
                buttonText,
                style: TextStyle(
                  color: textColor,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
